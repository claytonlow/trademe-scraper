window.pageCount = 1;
var soldCount = 0;
var totalPrice = 0;
var sellers = [];
var sellerCount = [];
var dateRange = [];
var domain = 'http://www.trademe.co.nz';
var tableTemplate = `
	<div id="manipulatedContainer">
		<div id="heading-bar" style="border-top:1px solid #4B4B4B; border-bottom:1px solid #4B4B4B; padding:10px 0 2px; margin-bottom:5px; color:#4B4B4B;">
			
			<div class="pull-left" id="listingInfo" style="font-weight:bold;"></div>
			<div class="pull-right">Days Range: <span id="daysRange"></span></div>
			<div class="clearfix clear"></div>
		</div>
		
		<div id="statistics-container">
			<div id="statistics">
				<div class="row">
					<div class="col-xs-12 col-sm-3 col-md-2">
						<div class="panel panel-default">
							<div class="panel-heading">
								Successful Listings
							</div>
							
							<div class="panel-body">
								<h3 id="soldItemsCount"></h3>
							</div>
						</div>			
					</div>
					
					<div class="col-xs-12 col-sm-3 col-md-2">
						<div class="panel panel-default">
							<div class="panel-heading">
								Average Price
							</div>
							
							<div class="panel-body">
								<h3 id="averagePrice"></h3>
							</div>
						</div>
					</div>
					
					<div class="col-xs-12 col-sm-3 col-md-2">
						<div class="panel panel-default">
							<div class="panel-heading">
								Total Sales
							</div>
							
							<div class="panel-body">
								<h3 id="totalPrice"></h3>
							</div>
						</div>
					</div>
					
					<div class="col-xs-12 col-sm-3 col-md-2">
						<div class="panel panel-default">
							<div class="panel-heading">
								Sell Through
							</div>
							
							<div class="panel-body">
								<h3 id="sellThroughRate"></h3>
							</div>
						</div>
					</div>
					
					<div class="col-xs-12 col-sm-3 col-md-2">
						<div class="panel panel-default">
							<div class="panel-heading">
								Top Seller
							</div>
							
							<div class="panel-body">
								<h3 id="topSeller"></h3>
							</div>
						</div>
					</div>
					
					<div class="col-xs-12 col-sm-3 col-md-2">
						<div class="panel panel-default">
							<div class="panel-heading">
								How many sellers
							</div>
							
							<div class="panel-body">
								<h3 id="sellerCount"></h3>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<table class="table sortable-theme-bootstrap" style="width:100%" data-sortable id="itemList">
			<thead>
				<tr>
					<th></th>
					<th>Item</th>
					<th>Unit Cost</th>
					<th>Qty Sold</th>
					<th>Total</th>
					<th>User</th>
					<th data-sortable-type="date">Date Sold</th>
					<th data-sortable-type="date">Time</th>
					<th>Views</th>
				</tr>
			</thead>
			<tbody>
				<td colspan="100%" class="text-center">
					<div style="text-align:center; margin:20px 0;" id="loader">
						<img src="http://omsys.funnel.assets.s3.amazonaws.com/images/default.gif" />
					</div>
				</td>
			</tbody>
		</table>
	</div>
`;

var tableList = '';
	
$(function() {
	$('#ListViewList').before(tableTemplate);
	$('#ListView_listingTableHeader_headerColumnListViewText').appendTo('#listingInfo');
	/*
	*	Adds in the search box
	*/
	$('#ListingsTitle_ListingTitleBar').prepend(`
		<div style="width:100%; padding:10px;">
			<form onsubmit="return false;">
				<div class="form-group">
					<input type="text" id="searchBox" placeholder="Search Expired Listings" class="form-control"/>
				</div>
				
				<div class="form-group" style="margin-bottom:0;">
					<button id="btnSearch" class="btn btn-default">Search</button> 
					<a class="btn btn-default" id="toggleView">Toggle View</a>
				</div>
			</form>
		</div>
		<hr />
	`);
	
	// Search Expired listings
	$('#btnSearch').on('click', function() {
		var val = $('#searchBox').val(),
			html = 'http://www.trademe.co.nz/Browse/SearchResults.aspx?from=advanced&advanced=true&searchstring=' + val + '&current=0&cid=0&rptpath=all&sort_order=expiry_desc&searchregion=100';
			
		window.location.href = html;		
	});
	
	// Toggle the view between custom and trademe view
	$('#toggleView').on('click', function() { $('#ListViewList').toggle(); $('#manipulatedContainer').toggle();});
	
	$('#searchBox').val(getParam('searchstring'));
	
	manipulate2();
	//getPageContent();
});

/*
*	Runs the manipulation of the page
*/
/*
var manipulate = function() {
	
	if ($('#ListViewList').length > 0) {
		var promises = [];
		
		// Buils the list from the listings given
		var buildList = function($listings) {
			$listings.each(function() {
				var $this = $(this),
					$link = $this.find('.listingTitle a');
				
				var link = $link.attr('href');
				
				$link.attr('target', '_blank');
				
				var request = $.get(link, function(r) {
					var dom = $.parseHTML(r),
						text = $(dom).find('.expired-listing-heading').text(),
						closedText = $(dom).find('.closing-time-content'),
						isSold = text.indexOf('sold') > -1,
						qtySold = 1,
						price = $this.find('.listingBuyNowPrice').text(),
						price = price.length > 0 ? price : $this.find('.listingBidPrice').text(),
						image = $this.find('.listingImage img').attr('src'),
						user = $(dom).find('#SellerProfile_MemberNicknameLink').text();
					
					var matches = text.match(/[0-9]+ item/gi);
					
					if (matches !== null && matches.length > 0) {
						qtySold = matches[0].replace(" item","");
					}
										
					if (isSold) {
						var totalSale = (parseFloat(price.replace("$", "")) * parseFloat(qtySold));
						totalPrice += totalSale;
						
						soldCount += parseInt(qtySold);
						
						sellers.push(user);
						
						tableList += `
							<tr>
								<td class="text-center">
									<img src="` + image + `" class="img-responsive"/>
								</td>
								<td>
									<a href="` + link + `" target="_blank">` + $link.text() + `</a>
								</td>
								<td>` + price + `</td>
								<td>` + qtySold + `</td>
								<td>$` + totalSale.toFixed(2) + `</td>
								<td><a href="javascript:;" class="searchMember">` + user + `</a></td>
								<td >` + closedText.find('span').text().replace('Closed: ', '').replace('This auction used auto-extend.', '') + `</td>
							</tr>
						`;
					}
				});
				
				promises.push(request);
			});
		};
		
		var $listings = $('#ListViewList > li');
		
		buildList($listings);
	}
	
	$.when.apply(null, promises).done(function() {
		$('#itemList tbody').html(tableList);
		
		//	How many items sold and for how much
		$('#soldItemsCount').html(soldCount);
		$('#totalPrice').html('$' + totalPrice.toFixed(2));
		
		// Average price per unit
		$('#averagePrice').html('$' + (parseInt(totalPrice) / parseInt(soldCount)).toFixed(2));
		var totalListings = parseInt($('.listing-count-holder').text().split(' ')[0]);
		
		$('#sellThroughRate').html(((parseInt(soldCount)/parseInt(totalListings))*100).toFixed(2) + '%');
		
		var topSeller = mode(sellers);
		
		$('#topSeller').html('<a href="javascript:;" class="searchMember">' + topSeller + '</a>');
		$('.searchMember').off().on('click', function() {
			$('#generalSearch').attr('target','_blank');
			$('#searchString').val(topSeller);
			$('#SearchType').val('Seller');
			$('#generalSearch').attr('target','_blank').submit();
		});
		Sortable.init();
		
		
	});
}*/
	
function manipulate2() {
	var promises = [];
	
	var count = parseInt($('.listing-count-holder').text().split(' ')[0]);
	var maxCount = Math.ceil(count / 60);
	var searchQuery = $('#searchBox').val();
	
	scrape(maxCount, searchQuery, function(pages) {
		scrape2(pages, function(data) {
			$.each(data, function(k, detail) {
				var link = detail.link,
					heading = detail.heading;
				
				var request = $.get(link, function(r) {
					html = r.replace(/<img[^>]*>/g,"");
					
					var dom = $.parseHTML(html),
						text = $(dom).find('.expired-listing-heading').text(),
						closedText = $(dom).find('.closing-time-content'),
						user = $(dom).find('#SellerProfile_MemberNicknameLink').text(),
						isSold = text.indexOf('sold') > -1,
						qtySold = 1,
						price = detail.price,
						image = detail.image,
						dateSold = closedText.find('span').text().replace('Closed: ', '').replace('This auction used auto-extend.', ''),
						pageViews = parseInt($(dom).find('.page-count').text().replace(/\s/g, ""));
					
					sellerCount.push(user);
					
					var matches = text.match(/[0-9]+ item/gi);
					
					if (matches !== null && matches.length > 0) {
						qtySold = matches[0].replace(" item","");
					}
										
					if (isSold) {
						var totalSale = (parseFloat(price.replace("$", "")) * parseFloat(qtySold));
						totalPrice += totalSale;
						
						soldCount += parseInt(qtySold);
						
						sellers.push(user);
						
						//dateRange.push(moment(dateSold.split(',')[0] + ' 2016').format('Do MMM'));					
						dateRange.push(moment(dateSold.split(',')[0] + ' 2016'));					
						
						tableList += `
							<tr>
								<td class="text-center">
									<img src="` + image + `" class="img-responsive"/>
								</td>
								<td>
									<a href="` + link + `" target="_blank">` + heading + `</a>
								</td>
								<td>` + price + `</td>
								<td>` + qtySold + `</td>
								<td>$` + totalSale.toFixed(2) + `</td>
								<td><a href="javascript:;" class="searchMember">` + user + `</a></td>
								<td >` + dateSold.split(",")[0] + `</td>
								<td >` + dateSold.split(", ")[1] + `</td>
								<td >` + pageViews + `</td>
							</tr>
						`;
					}
				});
				
				promises.push(request);
			});
				
			$.when.apply(null, promises).done(function() {
				$('#itemList tbody').html(tableList);
				
				//	How many items sold and for how much
				$('#soldItemsCount').html(soldCount);
				$('#totalPrice').html('$' + totalPrice.toFixed(2));
				
				// Average price per unit
				$('#averagePrice').html('$' + (parseInt(totalPrice) / parseInt(soldCount)).toFixed(2));
				var totalListings = parseInt($('.listing-count-holder').text().split(' ')[0]);
				
				$('#sellThroughRate').html(((parseInt(soldCount)/parseInt(totalListings))*100).toFixed(2) + '%');
				
				var topSellerDetail = mode(sellers);
				var topSeller = topSellerDetail.user;
				var sellCount = topSellerDetail.sellCount;
				
				$('#topSeller').html('<a href="javascript:;" class="searchMember">' + topSeller + ' ('+ sellCount +')</a>');
				$('.searchMember').off().on('click', function() {
					$('#generalSearch').attr('target','_blank');
					$('#searchString').val($(this).text());
					$('#SearchType').val('Seller');
					$('#generalSearch').attr('target','_blank').submit();
				});
				
				$('#sellerCount').html(_.uniq(sellerCount).length);
				
				var dates = [];
				$.each(dateRange, function(k,v) {
					dates.push(moment(v, "Do MMM").unix());
				});
				
				//var dates = _.uniq(dateRange);
				
				
				dates.sort(function(a,b){
				  // Turn your strings into dates, and then subtract them
				  // to get a value that is either negative, positive, or zero.
				  //return new Date(b.date) - new Date(a.date);
				  return b - a;
				});
				
				//var daysRange = daydiff(dates[0], dates[dates.length -1]);
				
				var daysRange = '<strong>' + moment(dates[dates.length -1] * 1000).format("ddd Do MMM") + '</strong> to <strong>' + moment(dates[0] * 1000).format("ddd Do MMM") + '</strong>';
				
				$('#daysRange').html(daysRange);
				
				Sortable.init();	

				var waypoint = new Waypoint({
				  element: document.getElementById('statistics'),
				  handler: function(direction) {
					if (direction == 'down') {
						$('#statistics').addClass('fixed');
					} else {
						$('#statistics').removeClass('fixed');
					}
				  }
				})
			});
		});
	});
}

function scrape(total_page, searchQuery, callback) {
	var pages = [];
	
	for (i=1;i<=total_page;i++) {
		var paramsObj = {
			cid: 0,
			searchType: '',
			searchString: searchQuery,
			x: 0,
			y: 0,
			searchregion: 100,
			type: 'Search',
			sort_order: 'expiry_desc',
			redirectFromAll: 'False',
			rptpath: 'all',
			page: i,
			advanced: 'true',
			current: 0
		};
		
		var params = Object.keys(paramsObj).map(function(key) {
			return key + '=' + paramsObj[key];
		}).join('&');
		
		pages.push({ 'url': domain + '/Browse/SearchResults.aspx?' + params});
	}
	
	callback(pages);
}

function scrape2(pages, cb) {
	var pageCount = 1,
		data = [],
		maxPages = pages.length,
		promises = [];
	
	$.each(pages, function (key, page) {
		$.get(page.url, function(html) {
			var html = $.parseHTML( html);
			
			$(html).find('#ListViewList > li').each(function () {
				var $this = $(this),
					$title = $this.find('.listingTitle a.dotted'),
					price = $this.find('.listingBuyNowPrice').text(),
					price = price.length > 0 ? price : $this.find('.listingBidPrice').text(),
					price = price.length > 0 ? price : $this.find('.current-bid-details').text(),
					image = $this.find('.listingImage img').attr('src'),
					link = 'http://www.trademe.co.nz' + $title.attr('href');
			
				data.push({ 'link' : link, 'heading': $title.text(), 'price': price, 'image': image});
			});
			
			if (pageCount == maxPages) {
				cb(data);
			}
			
			pageCount++;
		});
	});
	
}

function getPageContent(pageNo, html) {
	manipulate2();
	return false;
	html = typeof(html) === 'undefined' ? '' : html;
	
	//var maxCount = $($('#PagingFooter a')[$('#PagingFooter a').length -2]).text();
	var count = parseInt($('.listing-count-holder').text().split(' ')[0]);
	var maxCount = Math.ceil(count / 60);
	pageNo = typeof(pageNo) === 'undefined' ? 2 : pageNo;
	
	if (pageNo <= maxCount) {
		var val = $('#searchBox').val(),
			url = 'http://www.trademe.co.nz/Browse/SearchResults.aspx?cid=0&searchType=&searchString='+ val +'&x=0&y=0&searchregion=100&type=Search&sort_order=expiry_desc&redirectFromAll=False&rptpath=all&page='+ pageNo +'&advanced=true&current=0';
		
		$.get(url, function(k,v) { console.log(v); }).complete(function(data) { 
			console.log('loaded page ' + pageNo);
			var dom = $.parseHTML(data.responseText),
				newHtml = $(dom).find('#ListViewList').html();
				
			html += newHtml;
			
			getPageContent(pageNo +1 , html);
		});
		
		
	} else {
		$('#ListViewList').append(html);
		
		console.log('got pages, manipulate that shit');
		
		manipulate();
	}
}


function mode(array)
{
    if(array.length == 0)
    	return null;
    var modeMap = {};
    var maxEl = array[0], maxCount = 1;
    for(var i = 0; i < array.length; i++)
    {
    	var el = array[i];
    	if(modeMap[el] == null)
    		modeMap[el] = 1;
    	else
    		modeMap[el]++;	
    	if(modeMap[el] > maxCount)
    	{
    		maxEl = el;
    		maxCount = modeMap[el];
    	}
    }
    return { user: maxEl, sellCount: maxCount};
}

function getParam(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function parseDate(str) {
    var mdy = str.split('/');
    return new Date(mdy[2], mdy[0]-1, mdy[1]);
}

function daydiff(first, second) {
    return Math.round((second-first)/(1000*60*60*24));
}
